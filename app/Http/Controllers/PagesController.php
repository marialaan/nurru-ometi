<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;

class PagesController extends Controller
{

    public function index() {
        
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.index')->with('posts', $posts);
    }

    public function contact() {
        return view('pages.contact');
    }

    public function about() {
        return view('pages.about');
    }

    public function adopting() {
        return view('pages.adopting');
    }

    public function news() {
        return view('pages.news');
    }

    public function looking() {
        
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        return view('pages.looking')->with('posts', $posts);
    }

/*     public function profile() {

        $post = Post::find($id);
        return view('posts.show')->with('post', $post);   
    } */

    public function admin() {
        return view('pages.admin');
    }

    public function login() {
        return view('pages.login');
    }
}
