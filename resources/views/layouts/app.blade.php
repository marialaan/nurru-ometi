<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{config('app.name', 'LSAPP')}}></title>
    <link href="{!! asset('css/app.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
</head>
<body class="template-{{ collect(\Request::segments())->implode('-') }}">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=1496781560366123';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<article>
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".header-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/laravel/public">
                        <img src="{{url('/images/nurru-ometi-logo.svg')}}" alt="Nurru ometi logo" width="150" height="58">
                    </a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/laravel/public" class="active-menu-item">Avaleht</a></li>
                    <li><a href="/laravel/public/uudised">Uudised</a></li>
                    <li><a href="/laravel/public/kodu-otsivad-loomad">Kodu otsivad loomad</a></li>
                    <li><a href="/laravel/public/pakun-hoiukodu">Pakun hoidukodu</a></li>
                    <li><a href="/laravel/public/meist">Meist</a></li>
                    <li><a href="/laravel/public/kontakt">Kontakt</a></li>
                </ul>
                <div class="nav navbar-nav navbar-right">
                    <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal">
                        Anneta
                    </button>
                </div>
            </div>
        </nav>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p class="modal-title" id="myModalLabel">Anneta</p>
                    </div>
                    <div class="modal-body">
                        <p>Lorem Ipsum is simply dummy text</p>
                        <p class="type04">Sisesta summa: </p>
                        <input class="form-control form-control-sm" type="text" placeholder="25€">
                        <ul class="list-inline">
                            <li class="item02"><a class="ico-swed" href="#"><span class="sr-only">Swedbank</span></a></li>
                            <li class="item02"><a class="ico-seb" href="#"><span class="sr-only">SEB</span></a></li>
                            <li class="item02"><a class="ico-lhv" href="#"><span class="sr-only">LHV</span></a></li>
                        </ul>
                        <br />
                        <p class="modal-title" id="myModalLabel">Kuhu annetused lähevad?</p>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
                    </div>
                </div>
            </div>
        </div><!-- /.modal -->
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <ul class="list-inline">
                            <li>E-post: <a href="mailto:info@nurruometi.ee">info@nurruometi.ee</a></li>
                            <li>Telefon: <a href="tel:555 555">555 555</a></li>
                            <li>Kontonumber:  EE052200221062271245</li>
                        </ul>
                    </div>
                    <div class="col-md-5">
                        <div class="input-group">
                            <form action="">
                                <input type="text" class="form-control" placeholder="Sisesta otsingusõna">
                                <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="button">Otsi</button>
                                    </span>
                            </form>
                        </div>
                        <a class="ico-fb" href="https://www.facebook.com/nurruometi/">Facebook</a>
                    </div>
                </div>
            </div>
        </div>
    </header>


        @yield('content')


    <footer>
        <section>
            <div class="bg02">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="fb-page" data-href="https://www.facebook.com/nurruometi/" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/nurruometi/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/nurruometi/">Nurru Ometi</a></blockquote></div>
                        </div>
                        <div class="col-md-6">
                            <div class="content">
                                <div>
                                    <h3 class="type03"><strong>Kasulikud viited:</strong></h3>
                                    <ul>
                                        <li><a href="#">Loomavõtja ankeet</a></li>
                                        <li><a href="#">Paku hoiukodu</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="footer-bottom">
                <div class="container">
                    <div class="footer-bottom__content">
                        <ul>
                            <li>E-post: <a href="mailto:info@nurruometi.ee">info@nurruometi.ee</a></li>
                            <li>Telefon: <a href="tel:555 555">555 555</a></li>
                            <li>Kontonumber:  EE052200221062271245</li>
                        </ul>
                        <a class="navbar-brand" href="#">
                            <img src="{{url('/images/nurru-ometi-logo.svg')}}" alt="Nurru ometi logo" width="100" height="38">
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </footer>
</article>
</body>
<script type="text/javascript" src="{!! asset('js/jquery.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
</html>
