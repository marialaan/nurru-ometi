@extends('layouts.app')


@section('content')

    <article>
        <div class="box00">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content">
                            <ul class="list-inline list01">
                                <li><a href="./#contact-form" class="btn btn-green text-center">Soovin adopteerida</a></li>
                                <li><a href="./#contact-form" class="btn btn-blue text-center">Küsin lisainfot</a></li>
                            </ul>
                            <div class="img-wrap">
                                <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="500px" height="auto">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content">
                            <ul class="list-inline">
                                <li class="item01"><a class="ico-fb" href="#"><span class="sr-only">Facebook</span></a></li>
                                <li class="item01"><a class="ico-msn" href="#"><span class="sr-only">Messenger</span></a></li>
                                <li class="item01"><a class="ico-gmail" href="#"><span class="sr-only">Gmail</span></a></li>
                            </ul>
                            <h3 class="type01">{{$post->name}}</h3>
                            <ul class="list03">
                                <li>Vanus: {{$post->age}}</li>
                                <li>Sugu: {{$post->gender}}</li>
                                <li>Asukoht: {{$post->location}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box05">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="box02 text-center">
            <div class="container">
                <h2 class="type01 line-btm">Vaata veel teisi kodu otsivaid loomi</h2>
                <ul class="list00 row">              
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </article>
    <article>
        <div class="box02 text-center">
            <div class="container">
                <h2 class="type01 line-btm">Viimati vaadatud loomad</h2>
                <ul class="list00 row">
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4">
                        <div class="circle">
                            <a href="/laravel/public/loomaprofiil"><span class="sr-only">Miisu</span></a>
                        </div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                    <defs>
                                        <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                    </defs>
                            <circle cx="150" cy="150" r="150" fill="none"/>
                            <g>
                                <use xlink:href="#criclePath" fill="none"/>
                                <text y="180" fill="#fff">
                                    <textPath startOffset="10px" xlink:href="#criclePath">Miisu</textPath>
                                </text>
                            </g>
                                </svg>
                        <div class="img-wrap">
                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                            <div class="overlay">
                                <p>Vanus: 1-aastane</p>
                                <p>Asukoht: Tallinn</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </article>
    
@endsection 