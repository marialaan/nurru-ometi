@extends('layouts.app')

@section('content')

    <article>
        <div class="front-hero front-hero--subpage" style="background: linear-gradient( rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.9)), url('{{url('/images/5507692-cat-m.jpg')}}'); background-size: cover; background-position: 0 50%;">
            <div class="container">
                <h2 class="type02 line-btm">Meist</h2>
            </div>
        </div>
    </article>
    <article>
        <div class="box00">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <p class="text00 align-left">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

@endsection 