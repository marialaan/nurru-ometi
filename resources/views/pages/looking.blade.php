@extends('layouts.app')

@section('content')

    <article>
        <div class="front-hero front-hero--subpage" style="background-image: url('{{url('/images/chris-lawton-376574.jpg')}}');">
            <div class="container">
                <h2 class="type02 line-btm">Kodu otsivad loomad</h2>
            </div>
        </div>
    </article>
    <article>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box02 text-center">
                        <ul class="list00 row">

                            @if(count($posts) > 0)
                                @foreach($posts as $post)

                                    <li class="col-md-6">
                                        <div class="circle">
                                            <a href="/laravel/public/posts/{{$post->id}}"><span class="sr-only">{{$post->name}}</span></a>
                                        </div>
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                            <defs>
                                                <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                            </defs>
                                            <circle cx="150" cy="150" r="150" fill="none"/>
                                            <g>
                                                <use xlink:href="#criclePath" fill="none"/>
                                                <text y="180" fill="#fff">
                                                    <textPath startOffset="10px" xlink:href="#criclePath">{{$post->name}}</textPath>
                                                </text>
                                            </g>
                                        </svg>
                                        <div class="img-wrap">
                                            <img src="{{url('/images/bg02.png')}}" alt="Kassi pilt" width="250" height="250">
                                            <div class="overlay">
                                                <p>Vanus: {{$post->age}}-aastane</p>
                                                <p>Asukoht: {{$post->location}}</p>
                                            </div>
                                        </div>
                                    </li>

                                    @endforeach
    
                                    {{$posts->links()}}
                
                                @else
                                <p>No posts found</p>
                            @endif

                        </ul>
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="box00">
                    <div class="col-md-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Sugu</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="radio">
                                                <label><input type="radio" name="optradio">Emane</label>
                                            </div>
                                            <div class="radio">
                                                <label><input type="radio" name="optradio">Isane</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Vanus</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">&lt; 1</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">&gt; 1</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Asukoht</h3>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" id="exampleFormControlSelect1">
                                                <option>Tallinn</option>
                                                <option>Tartu</option>
                                                <option>Pärnu</option>
                                                <option>Võru</option>
                                                <option>Rakvere</option>
                                            </select>
                                        </div>
                                    </div>
                                    <a href="#" class="btn btn-success">Filtreeri tulemused</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

@endsection 