@extends('layouts.app')


@section('content')

    <article>
        <div class="front-hero" style="background-image: url('{{url('/images/chris-lawton-376574.jpg')}}');">
            <div class="container">
                <a href="/laravel/public/meist" class="front-hero__title">
                    <h1><span>Aita abistada </span><span>hätta sattunud lemmikloomi </span><span>Edendame üheskoos Eesti loomapidamiskultuuri</span></h1>
                </a>
            </div>
        </div>
        <span id="scroll-down" class="scroll-btn">
	        <a href="#next-section"><span class="mouse"><span></a>
            <p>Liigu alla</p>
        </span>
    </article>
    <article>
        <div id="next-section" class="box02 text-center">
            <div class="container">
                <h2 class="type01 line-btm">Kodu otsivad loomad</h2>
                <ul class="list00 row">
                    
                    @if(count($posts) > 0)
                        @foreach($posts as $post)
    
                        <li class="col-md-4">
                            <div class="circle">
                                <a href="/laravel/public/posts/{{$post->id}}"><span class="sr-only">{{$post->name}}</span></a>
                            </div>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="350px" height="350px" viewBox="0 0 300 300" enable-background="new 0 0 300 300" xml:space="preserve">
                                        <defs>
                                            <path id="criclePath" d=" M 150, 150 m -120, 0 a 120,120 0 0,1 240,0 a 120,120 0 0,1 -240,0 "/>
                                        </defs>
                                <circle cx="150" cy="150" r="150" fill="none"/>
                                <g>
                                    <use xlink:href="#criclePath" fill="none"/>
                                    <text y="180" fill="#fff">
                                        <textPath startOffset="10px" xlink:href="#criclePath">{{$post->name}}</textPath>
                                    </text>
                                </g>
                                    </svg>
                            <div class="img-wrap">
                                <img src="{{url('/images/cat04.jpg')}}" alt="Kassi pilt" width="250" height="250">
                                <div class="overlay">
                                    <p>Vanus: {{$post->age}}</p>
                                    <p>Asukoht: {{$post->location}}</p>
                                </div>
                            </div>
                        </li>
    
                        @endforeach
    
                        {{$posts->links()}}
    
                    @else
                    <p>No posts found</p>
                    @endif

                </ul>
                <a href="/laravel/public/kodu-otsivad-loomad" class="btn btn-green text-center">Vaata kõiki kodu otsivaid loomi</a>
            </div>
        </div>
    </article>
    <article>
        <div class="box00 img-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="text-center">
                            <h2 class="type01 line-btm">Vajame abi</h2>
                            <p class="text00">Me ei jaksa enam tegeleda tagajärgedega - õnnetute hüljatud neljajalgsete perekonnaliikmetega, kes peavad tänavatel iga päikesetõusu nimel võitlema</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="box03">
            <div class="container">
                <div class="text-center">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="type02 line-btm text-center">Vabatahtlike ühing</h2>
                            <p class="text00">Me tahame anda oma panuse Eesti lemmikloomapidamiskultuuri edendamisse, et inimese tegevuse või tegevusetuse pärast ei peaks hukkuma enam tuhanded ja tuhanded süütud hinged.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="box00">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="content">
                            <h2 class="type01">Kuidas saad sina aidata loomi?</h2>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                            <a href="/laravel/public/pakun-hoiukodu" class="btn btn-green">Paku hoiukodu</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <figure>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/L8uinCmOu0s" frameborder="0" allowfullscreen></iframe>
                            <figcaption>A rabid unicorn goring a fairy.</figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="box02" style="background-image: url('{{url('/images/bg02.png')}}');">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-push-6 bg00 pad01">
                        <div class="w-550">
                            <h2 class="type02">Unistus! Steriliseerimisprojekt!</h2>
                            <h3>Visioon</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. commodo consequat. </p>
                            <h3>Abi, mida vajame</h3>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. commodo consequat. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article>
        <div class="container">
            <section id="contact-form">
                <div class="box04">
                    <div class="row">
                        <div class="text-center">
                            <h2 class="type01 line-btm text-center">Võta meiega ühendust!</h2>
                            <div class="form-container">
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nimi*" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="E-post*" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Pealkiri*" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="7" placeholder="Teade*" required></textarea>
                                    </div>
                                    <button type="submit" class="btn-send"><span>Saada kiri</span></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>

@endsection