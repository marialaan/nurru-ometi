@extends('layouts.app')

@section('content')

    <article>
        <div class="front-hero front-hero--subpage" style="background-image: url('{{url('/images/chris-lawton-376574.jpg')}}');">
            <div class="container">
                <h2 class="type02 line-btm">Logi sisse</h2>
            </div>
        </div>
        <div class="container">
            <div class="box02">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputUser3" class="sr-only">Kasutaja</label>
                                <input type="text" class="form-control" id="inputUser3" placeholder="Kasutaja" required>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="sr-only">Parool</label>
                                <input type="password" class="form-control" id="inputPassword3" placeholder="Parool" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-green btn-block no-margin">Logi sisse</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </article>

@endsection