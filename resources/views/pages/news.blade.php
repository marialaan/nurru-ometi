@extends('layouts.app')

@section('content')

    <article>
        <div class="front-hero front-hero--subpage" style="background: linear-gradient( rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.9)), url('{{url('/images/maxresdefault.jpg')}}'); background-size: cover; background-position: 0 20%;">
            <div class="container">
                <h2 class="type02 line-btm">Uudised</h2>
            </div>
        </div>
    </article>
    <article>
        <div class="container">
            <div class="box00">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <a href="#"><img src="{{url('/images/bg02.png')}}" alt="Uudise pilt"/></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="content">
                                    <a href="#">
                                        <h2>Lorem ipsum dolor sit amet</h2>
                                    </a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut condimentum eget nunc quis semper. Nunc pellentesque nec libero sed viverra. Donec turpis velit, suscipit sit amet mauris quis, viverra ornare massa. Morbi tellus eros, fringilla eu sodales id, ultricies ut sapien. Duis et tristique enim, vitae mattis neque. Aliquam efficitur, dui id laoreet blandit, lorem […]</p>
                                    <a href="#">
                                        <button type="button" class="btn btn-blue">Loe rohkem</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <a href="#"><img src="{{url('/images/bg02.png')}}" alt="Uudise pilt"/></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="content">
                                    <a href="#">
                                        <h2>Lorem ipsum dolor sit amet</h2>
                                    </a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut condimentum eget nunc quis semper. Nunc pellentesque nec libero sed viverra. Donec turpis velit, suscipit sit amet mauris quis, viverra ornare massa. Morbi tellus eros, fringilla eu sodales id, ultricies ut sapien. Duis et tristique enim, vitae mattis neque. Aliquam efficitur, dui id laoreet blandit, lorem […]</p>
                                    <a href="#">
                                        <button type="button" class="btn btn-blue">Loe rohkem</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div class="thumbnail">
                                    <a href="#"><img src="{{url('/images/bg02.png')}}" alt="Uudise pilt"/></a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="content">
                                    <a href="#">
                                        <h2>Lorem ipsum dolor sit amet</h2>
                                    </a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut condimentum eget nunc quis semper. Nunc pellentesque nec libero sed viverra. Donec turpis velit, suscipit sit amet mauris quis, viverra ornare massa. Morbi tellus eros, fringilla eu sodales id, ultricies ut sapien. Duis et tristique enim, vitae mattis neque. Aliquam efficitur, dui id laoreet blandit, lorem […]</p>
                                    <a href="#">
                                        <button type="button" class="btn btn-blue">Loe rohkem</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

@endsection 