@extends('layouts.app')

@section('content')

    <article>
        <div class="front-hero front-hero--subpage" style="background: linear-gradient( rgba(0, 0, 0, 0),rgba(0, 0, 0, 0.9)), url('{{url('/images/chris-lawton-376574.jpg')}}'); background-size:cover; background-position: 0 70%;">
            <h2 class="type02 line-btm">Kontaktandmed</h2>
        </div>
    </article>
    <article>
        <div class="box00">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="content text-center">
                            <ul class="list-inline">
                                <li>E-post: <a href="mailto:info@nurruometi.ee">info@nurruometi.ee</a></li>
                                <li>Telefon: <a href="tel:555 555">555 555</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <article class="article00">
        <div class="container">
            <section>
                <div class="box04">
                    <div class="row">
                        <div class="text-center col-md-7">
                            <div class="form-container">
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nimi*" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="E-post*" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Pealkiri*" required>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" rows="7" placeholder="Teade*" required></textarea>
                                    </div>
                                    <button type="submit" class="btn-send"><span>Saada kiri</span></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="content">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                <a href="#" class="btn btn-blue">Tee test siin</a>
                            </div>
                            <div class="accordion" id="accordion" role="tablist">
                                <h3 class="accordion-heading">Tingimused</h3>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingOne">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                Collapsible Group Item #1
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                        <div class="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingTwo">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Collapsible Group Item #2
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                        <div class="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" role="tab" id="headingThree">
                                        <h5 class="mb-0">
                                            <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Collapsible Group Item #3
                                            </a>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                        <div class="card-body">
                                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </article>

@endsection 