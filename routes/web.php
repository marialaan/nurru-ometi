<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/kontakt', 'PagesController@contact');

Route::get('/meist', 'PagesController@about');

Route::get('/kodu-otsivad-loomad', 'PagesController@looking');

Route::get('/pakun-hoiukodu', 'PagesController@adopting');

Route::get('/uudised', 'PagesController@news');

Route::get('/loomaprofiil', 'PagesController@profile');

Route::get('/admin', 'PagesController@admin');

Route::get('/sisene', 'PagesController@login');

Route::resource('posts', 'PostsController');

Route::resource('articles', 'ArticlesController');

View::composer('*', function ($view) {
    View::share('viewName', $view->getName());
});


